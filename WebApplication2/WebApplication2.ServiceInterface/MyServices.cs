﻿using ServiceStack;
using WebApplication2.ServiceModel;


namespace WebApplication2.ServiceInterface
{
    public class MyServices : Service
    {   
        public IMongoDatabase MongoDb { get; set; }

        public object Any(Hello request)
        {
            return new HelloResponse { Result = "Hello, {0}!".Fmt(request.Name) };
        }

        public object Post(User request)
        {

            var usersCollection = MongoDb.GetCollection<User>("Users");
            usersCollection.Insert(new User { Name = request.Name, Surname = request.Surname });

            return new UserResponse { Result = "User: "+request.Name+" "+request.Surname+ " was added to database!" };
        }

        public object Get(User request)
        {
            var usersCollection = MongoDb.GetCollection<User>("Users");

            if (usersCollection.find(new User { Name = request.Name, Surname = request.Surname }))
                return new UserResponse { Result = "User exist in database already!" };

            else
                return Post(request);
        }

        
    }
}
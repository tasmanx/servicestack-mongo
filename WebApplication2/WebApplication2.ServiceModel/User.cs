﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;

namespace WebApplication2.ServiceModel
{
    [Route("/user/{Id}")]
    [Route("/user/{Name}/{Surname}")]
    public class User
    {
        public string Name { get; set; }
        public string Surname { get; set; }
    }

    public class UserResponse
    {
        public string Result { get; set; }
    }

    
}
